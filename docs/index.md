n3t Language Domains
====================

![Version](https://img.shields.io/badge/Latest%20version-5.0.2-informational)
![Release date](https://img.shields.io/badge/Release%20date-2024--04--12-informational)

n3t Language Domains allows to assign language per domain. This means you can have for
example `example.com` with english content, `example.cz` with czech content and
`example.de` with german content.

The plugin generates correct SEF or nonSEF links through whole site, and choose correct
language based on current domain. Also additional parameters (like Virtuemart currency
for example) could be defined for specific domain.
