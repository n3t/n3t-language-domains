Usage
=====

Prerequisities
--------------

First of all make your Joomla! site fully multilanguage. This means:
- enable core Language filter plugin
- publish your languages
- publish home page for each language
- publish Language module if needed

Details could be found in [Joomla! documenation](https://docs.joomla.org/J3.x:Setup_a_Multilingual_Site).

Make sure you multilingual site is working, before you start to use n3t Language Domains plugin.

Second, __make sure all domains you would like to use points same Joomla! installation__,
and there is no active redirection in your .htaccess file, redirecting one domain to another.

Installation
------------

n3t Language Domains could be installed as any other extension in Joomla! For detailed
information see Joomla documentation.

Do not forget to __enable and configure the plugin__ after installation.

Configuration
-------------

In configuration of n3t Language Domains plugin just add domain for every single
language you would like to use. Domain name has to be without http:// or https://
prefix, including www when applicable.

For example, if you own 3 domains:

- www.example.com - for english website
- www.example.cz - for czech website
- www.example.de - for german website

configuration would look like this:

![Configuration preview](images/configuration.jpg)

You are not limited in any way, how you choose your domain names, as long as it is fully qualified
domain name (FQDN). So you can use domains like in example above, or subdomains, like:

- en.example.com - for english website
- cz.example.com - for czech website
- de.example.com - for german website

or completely different domains like

- www.example.com - for english website
- www.priklad.cz - for czech website
- www.beispiel.de - for german website

Plugin ordering
---------------

If you still use Joomla 3, please make sure, that n3t Language Domains plugin is published __BEFORE__
core Language Filter plugin. In Joomla 4 and later this is handled automatically.

Additional parameters
---------------------

For each domain additional parameters could be specified. Just add Name - Value
couple to proper domain, and this will be automatically add to request. This could be
for example used to specify currency in your eshop for given language.

Languages without domain
------------------------

n3t Language Domains supports also languages without domain. Lats say you have 
three languages, but only 2 domains. All other languages, without specified domain in the plugin settings
will use the first specified domain (main domain) and use URL extension for the language specification, so:

- www.example.com - for english website
- www.example.cz - for czech website
- www.example.com/de - for geman website

Language domain redirection
---------------------------

If your site in specific language is accessed by different domain, then specified in the settings, user will get automatically
redirected to correct domain. This would improve your SEO, as only one domain per language will be acccessable.

This mean, when you try to access your website for example by URL www.example.com/cs you will be redirected automatically
to www.example.cz.

Joomla! versions
----------------

Currently only Joomla 3.10 and newer is supported.

More information could be found in [Release notes](about/release-notes.md)
