Credits
=======

Thanks to these projects and contributors:

- [Joomla! CMS][JOOMLA]
- [Czech Joomla community][JOOMLAPORTAL] for testing, support and comments on development

[JOOMLA]: https://www.joomla.org
[JOOMLAPORTAL]: https://www.joomlaportal.cz