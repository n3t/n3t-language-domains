Release notes
=============

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

[Unreleased]
------------

- no unreleased changes

[5.0.x]
-------

### [5.0.2] - 2024-04-12

#### Changed
- Redirect to main language domain, if language is accessed by another domain and doesn't have assigned domain

### [5.0.1] - 2024-03-20

#### Changed
- Languages without domain support
- Reordering of LanguageFilter parsing rule, now no need have correct plugins order (Joomla 4+)
- Redirect to specific language domain, if language is accessed by another domain

### [5.0.0] - 2024-01-23

#### Added
- Joomla 5 compatibility
#### Fixed
- invalid domain replacement for absolute links with non SSL protocol (http only)

[4.0.x]
-------

### [4.0.2] - 2023-02-26

#### Changed
- current domain is newly detected based on `HTTP_HOST` with fallback to `SERVER_NAME` HTTP header

### [4.0.1] - 2022-10-11

#### Changed
- Improved installer script
- minor code improvements

### [4.0.0] - 

- Initial public release
