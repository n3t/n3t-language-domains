<?php
/**
 * @package n3t Language Domains
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2020 - 2024 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

use Joomla\CMS\Application\CMSApplication;
use Joomla\CMS\Factory;
use Joomla\CMS\Language\LanguageHelper;
use Joomla\CMS\Plugin\CMSPlugin;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Router\Router;
use Joomla\CMS\Router\SiteRouter;
use Joomla\CMS\Uri\Uri;
use Joomla\CMS\Version;
use Joomla\Plugin\System\LanguageFilter\Extension\LanguageFilter;
use Joomla\String\StringHelper;

class plgSystemN3tLangDomains extends CMSPlugin
{

    protected $autoloadLanguage = true;

    private $app;
    private bool $isSef;
    private array $languages;
    private ?string $mainDomain = null;
    private array $langDomains = [];
    private array $domainLangs = [];
    private array $domainParams = [];

    public function __construct(&$subject, $config = [])
    {
        parent::__construct($subject, $config);

        $this->app       = Factory::getApplication();
        $this->isSef     = !!$this->app->get('sef', 0);
        $this->languages = LanguageHelper::getLanguages('sef');

        $languages = LanguageHelper::getLanguages('lang_code');
        foreach ($this->params->get('domains', []) as $domain) {
            if (isset($languages[$domain->language])) {
                if ($this->mainDomain === null)
                    $this->mainDomain = $domain->domain;
                $this->langDomains[$domain->language] = $domain->domain;
                $this->domainLangs[$domain->domain]   = $languages[$domain->language]->sef;
                $this->domainParams[$domain->domain]  = $domain->params;
            }
        }
    }

    public function onAfterInitialise()
    {
        if (!$this->app->isClient('site'))
            return;

        if (Version::MAJOR_VERSION >= 5) {
            $router = Factory::getContainer()->get(SiteRouter::class);
        } else {
            $router = CMSApplication::getInstance('site')->getRouter('site');
        }
        $router->attachBuildRule([$this, 'buildRuleBefore'], Router::PROCESS_BEFORE);
        $router->attachBuildRule([$this, 'buildRuleAfter'], Router::PROCESS_AFTER);
        $router->attachParseRule([$this, 'parseRuleBefore'], Router::PROCESS_BEFORE);
        $router->attachParseRule([$this, 'parseRuleAfter'], Router::PROCESS_AFTER);

        // make sure core LanguageFilter parseRule is parsed after n3t Language Domains
        if (Version::MAJOR_VERSION >= 4) {
            $rules = $router->getRules()['parsepreprocess'] ?? [];
            foreach ($rules as $rule) {
                if (is_array($rule) && count($rule) == 2 && is_a($rule[0], LanguageFilter::class)) {
                    $router->detachRule('parse', $rule, $router::PROCESS_BEFORE);
                    $router->attachParseRule($rule, $router::PROCESS_BEFORE);
                }
            }
        }
    }

    private function getCurrentDomain(): string
    {
        static $domain = null;

        if ($domain === null) {
            $domain = $this->app->input->server->get('HTTP_HOST');
            if (!$domain)
                $domain = $this->app->input->server->get('SERVER_NAME');
        }

        return $domain;
    }

    public function parseRuleBefore(&$router, &$uri)
    {
        $lang = null;

        // Check if the language is not specified
        if($this->isSef) {
            $path = $uri->getPath();
            $possibleLang = StringHelper::strtolower(explode('/', $path)[0]);
            if (isset($this->languages[$possibleLang])) {
                $lang = $possibleLang;
            }
        } else {
            $lang = $uri->getVar('lang');
        }

        if (!$lang) {
            $domain = $this->getCurrentDomain();
            if (isset($this->domainLangs[$domain])) {
                $uri->setVar('lang', $this->domainLangs[$domain]);
                foreach ($this->domainParams[$domain] as $param)
                    $uri->setVar($param->name, $param->value);
            }
        }
    }

    public function parseRuleAfter(&$router, &$uri) {
        $lang = $this->app->getLanguage()->getTag();
        if (
            (isset($this->langDomains[$lang]) && $this->langDomains[$lang] !== $this->getCurrentDomain())
            || (!isset($this->langDomains[$lang]) && $this->mainDomain !== $this->getCurrentDomain())
        ) {
            $redirectUri = clone $uri;
            $redirectUri->setPath('index.php');
            $redirectUrl = Route::_($redirectUri->toString(['path', 'query', 'fragment']));
            $redirectUrl = $uri->getScheme() . '://' . preg_replace('~/n3tlangdomains/~', '', $redirectUrl);
            $this->app->redirect($redirectUrl, 301);
        }
    }

    public function buildRuleBefore(&$router, &$uri)
    {
        $lang = $uri->getVar('lang', $this->app->getLanguage()->getTag());
        if ($lang && isset($this->languages[$lang]))
            $lang = $this->languages[$lang]->lang_code;

        if ($lang) {
            if (isset($this->langDomains[$lang]))
                $uri->setVar('n3tlangdomains', $this->langDomains[$lang]);
            else
                $uri->setVar('n3tlangdomains', $this->mainDomain);
        }
    }

    public function buildRuleAfter(&$router, &$uri)
    {
        if ($domain = $uri->getVar('n3tlangdomains')) {
            $uri->delVar('n3tlangdomains');
            $uri->delVar('lang');

            $path = $uri->getPath();
            // Remove path added by core plugin
            if (isset($this->domainLangs[$domain]))
                $path = preg_replace('~/' . $this->domainLangs[$domain] . '/~', '/', $path, 1);

            if ($domain !== $this->app->input->server->get('SERVER_NAME'))
                $uri->setPath('/n3tlangdomains/' . $domain . '/' . $path);
            else
                $uri->setPath($path);
        }
    }

    public function onAfterRender()
    {
        if ($this->app->isClient('administrator'))
            return;

        $buffer = $this->app->getBody();
        $buffer = preg_replace('~(https?://)([^/]+)/n3tlangdomains/~', '$1', $buffer);
        $buffer = preg_replace('~/n3tlangdomains/~', Uri::getInstance()->getScheme() . '://', $buffer);
        $this->app->setBody($buffer);
    }
}
