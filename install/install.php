<?php
/**
 * @package n3t Language Domains
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2020 - 2024 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined( '_JEXEC' ) or die( 'Restricted access' );

use Joomla\CMS\Factory;
use Joomla\CMS\Installer\InstallerScript;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Version;

class plgSystemN3tLangDomainsInstallerScript extends InstallerScript
{
	const LANG_PREFIX = 'PLG_SYSTEM_N3TLANGDOMAINS';

	protected $minimumJoomla = '3.10.0';
	protected $minimumPhp = '7.4.0';
	protected $allowDowngrades = true;
	protected $oldRelease = null;

  /**
   * @since 4.1.0
   * @inheritdoc
   */
  public function preflight($type, $parent)
  {
	  $return = parent::preflight($type, $parent);

	  if (strtolower($type) === 'update') {
			if (Version::MAJOR_VERSION === 3) {
				$manifest = $this->getItemArray('manifest_cache', '#__extensions', 'name', Factory::getDbo()->quote($this->extension));
			} else {
				$manifest = $this->getItemArray('manifest_cache', '#__extensions', 'name', $this->extension);
			}
		  if (isset($manifest['version'])) {
			  $this->oldRelease = $manifest['version'];
		  }
	  }

	  return $return;
  }

  /**
   * @since 4.0.0
   * @inheritdoc
   */
  public function update($parent)
  {
	  if ($this->oldRelease) {
		  Factory::getApplication()->enqueueMessage(Text::sprintf(self::LANG_PREFIX . '_INSTALL_UPDATE_VERSION', $this->oldRelease, $this->release));
	  }
	}

}
