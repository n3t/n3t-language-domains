n3t Language Domains
====================

![Version](https://img.shields.io/badge/Latest%20version-5.0.2-informational)
![Release date](https://img.shields.io/badge/Release%20date-2024--04--12-informational)

[![Joomla! CMS](https://img.shields.io/badge/Joomla!%20CMS-V3.10-green)][JOOMLA]
[![Joomla! CMS](https://img.shields.io/badge/Joomla!%20CMS-V4.x-green)][JOOMLA]
[![PHP](https://img.shields.io/badge/PHP-V7.4-green)][PHP]
[![Documentation Status](https://readthedocs.org/projects/n3t-language-domains/badge/?version=latest)][DOCS]

n3t Language Domains is system plugin allowing to specify language - domain dependency.

It means, if you have multilanguage site, you are able to specify domain for each language.
So for example `example.com` could point english language, `example.cz` czech and `example.de`
german.

Installation
------------

n3t Language Domains is Joomla! system plugin. It could be installed as any other extension in
Joomla!

After installing **do not forget to enable the plugin** from Plugin Manager in your
Joomla! installation and setup list of domains.

Documentation
-------------

Detailed documentation could be found at [n3t Language Domains documentation page][docs]

[DOCS]: https://n3t-language-domains.readthedocs.io
[JOOMLA]: https://www.joomla.org
[PHP]: https://www.php.net
